package pattern;

interface WordMaker {
    String returnWord(String input);
}
