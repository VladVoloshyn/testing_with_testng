package pattern;

class ProductName implements WordMaker {

    private String name;

    private void setName(String name) {
        this.name = name;
    }

    @Override
    public String returnWord(String input) {
        setName(input);
        return name;
    }
}
