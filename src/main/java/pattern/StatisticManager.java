package pattern;

import java.util.Date;
import java.util.concurrent.ConcurrentSkipListMap;

class StatisticManager {

    ConcurrentSkipListMap<Date, String> concurrent = new ConcurrentSkipListMap<>();

    void putData(String data) {
        Date currentDate = new Date();
        concurrent.put(currentDate, data);
    }

    void getStat() {
        System.out.println("Some statistical data:\n" + concurrent);
    }

    void getLastStat() {
        Date lastDate = concurrent.lastKey();
        System.out.println("Last report was:\n" + lastDate.toString() + " = " + concurrent.get(lastDate));
    }

}
