package pattern;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ConcurrentSkipListMap;

class FileManipulation {

    private String fileName = "";

    FileManipulation(String fileName) {
        this.fileName = fileName;
    }

    void writeToFile(ConcurrentSkipListMap<Date, String> map) {
        try {

            FileWriter fileWriter = new FileWriter(fileName, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            // every session's information will be on a separate line
            bufferedWriter.write(String.valueOf(map));
            bufferedWriter.newLine();

            bufferedWriter.close();

        } catch (FileNotFoundException ex) {
            System.out.println("File " + fileName + " not found");
        } catch (IOException ex) {
            System.out.println("Error writing file " + fileName);
        }

    }

}
