package pattern;

class SecondTypeOfResponse extends Response {

    @Override
    String chooseYourResponse() {
        String value = hash.get("family");
        return "Make your family " + value;
    }
}
