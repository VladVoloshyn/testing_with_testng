package pattern;

import java.util.HashMap;

abstract class Response {

    static HashMap<String, String> hash = new HashMap<String, String>() {{
        put("business", "TRUSTable");
        put("family", "HAPPY");
    }};

    abstract String chooseYourResponse();
}
