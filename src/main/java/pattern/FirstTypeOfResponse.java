package pattern;

class FirstTypeOfResponse extends Response {

    @Override
    String chooseYourResponse() {
        String value = hash.get("business");
        return "Get a profit from your business with " + value;
    }
}
