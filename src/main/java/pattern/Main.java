package pattern;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        CompanyName com = new CompanyName();
        ProductName prod = new ProductName();

        System.out.print("Enter the company name: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        System.out.print("What product you need to sell? : ");
        String product = scanner.nextLine();

        System.out.print("What type of motto should be generated ? (1 - For business, 2 - For Family) : ");

        StatisticManager statistic = new StatisticManager();
        FileManipulation file = new FileManipulation("report.txt");
        boolean flag = true;
        while (flag) {
            try {
                switch (scanner.nextInt()) {
                    case 1:
                        FirstTypeOfResponse first = new FirstTypeOfResponse();
                        System.out.println("Motto: " + first.chooseYourResponse() + " " + com.returnWord(input) + " / " + prod.returnWord(product));
                        statistic.putData(first.chooseYourResponse() + " " + com.returnWord(input) + " / " + prod.returnWord(product));
                        //flag = false;
                        break;
                    case 2:
                        SecondTypeOfResponse second = new SecondTypeOfResponse();
                        System.out.println("Motto: " + second.chooseYourResponse());
                        statistic.putData(second.chooseYourResponse());
                        flag = false;
                        break;
                    default:
                        System.out.print("You have entered wrong number\nPlease, try again : ");
                        break;
                }
            } catch (InputMismatchException ex) {
                System.out.println("Not a valid number!");
                System.exit(0);
            }
        }

        file.writeToFile(statistic.concurrent);

        statistic.getStat();
        statistic.getLastStat();
        System.out.println("Thank you, " + com.returnWord(input));

    }
}
