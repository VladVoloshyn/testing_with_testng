package pattern;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created in Intellij IDEA.
 * project: message-collections
 * author: Vladyslav Voloshyn
 * date: Sep 08, 2016
 * time: 20:33
 */
public class FileManipulationTest {

    private FileManipulation fileManipulation = new FileManipulation("file.txt");
    private ConcurrentSkipListMap<Date, String> listMap = new ConcurrentSkipListMap<>();
    private static String text;
    private static Date currentDate;
    private File f;

    @BeforeClass
    public void initMap() {

        f = new File("file.txt");

        currentDate = new Date();
        text = "REFACTORING";
        listMap.put(currentDate, text);
    }

    @Test
    public void checkWriteToFile() throws IOException {
        fileManipulation.writeToFile(listMap);
        Assert.assertNotNull(f);
    }
}
