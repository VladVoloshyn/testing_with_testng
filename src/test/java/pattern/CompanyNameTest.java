package pattern;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created in Intellij IDEA.
 * project: message-collections
 * author: Vladyslav Voloshyn
 * date: Sep 08, 2016
 * time: 20:25
 */
public class CompanyNameTest {

    private CompanyName companyName = new CompanyName();
    private final static String word = "blablabla";


    @Test
    public void checkReturnedWord() throws Exception {
        Assert.assertEquals(companyName.returnWord(word), word);
    }
}
