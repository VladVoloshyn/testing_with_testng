package pattern;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created in Intellij IDEA.
 * project: message-collections
 * author: Vladyslav Voloshyn
 * date: Sep 08, 2016
 * time: 19:14
 */
public class FirstTypeOfResponseTest {

    private FirstTypeOfResponse firstTypeOfResponse = new FirstTypeOfResponse();

    @Test
    public void testYourChoose() throws Exception {
        Assert.assertEquals("Get a profit from your business with TRUSTable", firstTypeOfResponse.chooseYourResponse());
    }
}
