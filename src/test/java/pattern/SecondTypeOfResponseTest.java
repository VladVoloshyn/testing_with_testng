package pattern;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created in Intellij IDEA.
 * project: message-collections
 * author: Vladyslav Voloshyn
 * date: Sep 08, 2016
 * time: 19:14
 */
public class SecondTypeOfResponseTest {

    private SecondTypeOfResponse secondTypeOfResponse = new SecondTypeOfResponse();

    @Test
    public void testYourChoose() throws Exception {
        Assert.assertEquals("Make your family HAPPY", secondTypeOfResponse.chooseYourResponse());
    }
}
