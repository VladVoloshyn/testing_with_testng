package pattern;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

/**
 * Created in Intellij IDEA.
 * project: message-collections
 * author: Vladyslav Voloshyn
 * date: Sep 08, 2016
 * time: 19:59
 */
public class StatisticManagerTest {

    private StatisticManager statisticManager = new StatisticManager();
    private static String data;

    @BeforeClass
    public void initSetTestVariables() {
        data = "testString";
        statisticManager.putData(data);
    }

    @Test
    public void putDataCheck() throws Exception {
        Assert.assertNotNull(statisticManager.concurrent);
        Assert.assertEquals(statisticManager.concurrent.values().toString(), "[" + data + "]");
    }

    @Test
    public void getLastStatCheck() throws Exception {
        Assert.assertEquals(statisticManager.concurrent.get(statisticManager.concurrent.lastKey()), data);
    }
}
