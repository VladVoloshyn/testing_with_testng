package pattern;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created in Intellij IDEA.
 * project: message-collections
 * author: Vladyslav Voloshyn
 * date: Sep 08, 2016
 * time: 20:25
 */
public class ProductNameTest {

    private ProductName productName = new ProductName();
    private final static String word = "kokokoko";

    @Test
    public void checkReturnedWord() throws Exception {
        Assert.assertEquals(productName.returnWord(word), word);
    }
}
